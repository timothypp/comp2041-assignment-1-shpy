#!/usr/bin/perl -w

#
#
# Created on 2 September 2015
# Last Update on 27 September 2015 1:30 am
# Subset Completed : 0, 1, 2, 3, 4 (except stdin & Here document)
#

use strict;

# Helper Variable
my $original_code_line = "";
my $line_number = 0;
my $indentation = 0;
my $case_variable = "";
my $in_case_statement = "no";

# Array Used
my @import_used = ();
my @code_array = ();
my @all_lines_separated = ();

# IFs test string comparator
my %expr_ops = (
	'=' => '==',
	'!' => '!=');

# IFs test boolean operator
my %test_seperators = (
	'-o' => 'or',
	'-a' => 'and');

# IFs test boolean operator
my %test_operators = (
	'||' => 'or',
	'&&' => 'and');

# IFs numberic test operator
my %numeric_test = (
	'-eq' => '==',
	'-ne' => '!=',
	'-gt' => '>',
	'-ge' => '>=',
	'-lt' => '<',
	'-le' => '<=');

# File output command
my %file_types = (
	'>>' => "'a'",
	'>' => "'w'");

# Debug Data
my $debug = "no"; 					# Print only the Original Code
my $turn_off_comment_line = "no"; 	# Prevent comment line from being printed
my $show_line_number = "no"; 		# Show the line number
my $should_do_final_print = "yes"; 	# Print the translated code





## All of the Main Functions ##

# Refactor all of the lines into individual lines as necessary
while (my $line = <>) {
    if ($line !~ /^#/ && $line !~ /^ *$/) {

        # $(  ) code, and converts $(( )) into  expr backticks (arithmetic code)
        $line =~ s/([^\']?)\$\(\((.*?)\)\)([^\']?)/$1\`expr $2\`$3/g;

        # $(  ) code, and converts $(  ) into  backticks
        $line =~ s/([^\']?)\$\((.*?)\)([^\']?)/$1\`$2\`$3/g;


        if ($line =~ /^\s*case (\S+) in/) {         # Check if a line is case
         	$case_variable = $1;                    # Grab the variable name use for case
	        $line =~ s/^\s*case \S+ in//;           # Remove the case line in a line
	        $in_case_statement = "yes";             # Notify that the code is in a case statement
	    }

        if ($in_case_statement eq "yes" && $line =~ /^\s*(\S+)[^\\]*\)/){ # If the code is in a case statement, then get the case option
	        push(@all_lines_separated, "fi"); # Push a fi command to the code array to restore the indetation level

            my $create_if = "if [ " . $case_variable . " == " . $1 . " ]"; # Create an IF statement
        	push(@all_lines_separated, $create_if); # Push the if statement into the code array

        	$line =~ s/^\s*([^\$])+[^\\]*\)//; # Remove the case option in a line;
			push(@all_lines_separated, "then"); # Push then command to increase the identation level
        }

        if ($in_case_statement eq "yes" && $line =~ /\;\;\s*$/) { # If there is a double semi-colon found and it is in an a case statement
	        $line =~ s/\;\;\s*$//; # Remove the double semi-colon
	        push(@all_lines_separated, "fi") if $line =~ /^\s*$/; # Push a fi command to the code array to restore the indetation level
	    }

        if ($in_case_statement eq "yes" && $line =~ /^\s*esac\s*$/) {
	        $in_case_statement = "no"; # Notify that the code is no longer in a case statement
	        $line =~ s/^\s*esac\s*$//; # Remove the esac in the line
	    }

	    if ($line =~ /^\s*$/){
	    	next;
	    }

        my @data = help_split_into_components($line, "no");

        foreach my $data_x (@data){
            $original_code_line .= $data_x;
            my $variable = $original_code_line;

		    # If, For and While line separator. It uses keyword such as Fi, Do, Done, Else, Then to separate it into
            # Different lines. It uses new line to separate the code into different parts.

            if ($original_code_line =~ /[^\\]\;$/){
                $original_code_line =~ s/^\s*//;
                $original_code_line =~ s/\s*$//;

                push (@all_lines_separated, "$original_code_line\n");

                $original_code_line =~ s/\Q$original_code_line\E//;
                next;
            }
            elsif ($variable =~ /\s(fi|do|done|else|then)\s/){ 
                my $statement = $1;
                $statement =~ s/^\s*//;
                $statement =~ s/\s*$//;

                $original_code_line =~ s/\s$statement\s//;
                $original_code_line =~ s/^\s*//;
                $original_code_line =~ s/\s*$//;

                my $storable = "\n" . $statement . "\n"  if defined $statement;

                push (@all_lines_separated, $storable);
            }
            elsif ($variable =~ /\s(fi|do|done|else|then)$/){
                my $statement = $1;
                $statement =~ s/^\s*//;
                $statement =~ s/\s*$//;

                $original_code_line =~ s/\s$statement$//;
                $original_code_line =~ s/^\s*//;
                $original_code_line =~ s/\s*$//;

                my $storable =  $statement . "\n" if defined $statement;

                push (@all_lines_separated, $storable);
            }
            elsif ($variable =~ /^(fi|do|done|else|then)\s/){
                my $statement = $1;
                $statement =~ s/^\s*//;
                $statement =~ s/\s*$//;

                $original_code_line =~ s/^$statement\s//;
                $original_code_line =~ s/^\s*//;
                $original_code_line =~ s/\s*$//;

                my $storable = "\n" . $statement if defined $statement;

                push (@all_lines_separated, $storable);
            }
            elsif ($variable =~ /^(fi|do|done|else|then)$/){
                my $statement = $1;
                $statement =~ s/^\s*//;
                $statement =~ s/\s*$//;

                $original_code_line =~ s/^$statement$//;
                $original_code_line =~ s/^\s*//;
                $original_code_line =~ s/\s*$//;

                my $storable = "\n" . $statement . "\n"  if defined $statement;

                push (@all_lines_separated, $storable);
            }          
        }

        $original_code_line =~ s/^\s*//;
        $original_code_line =~ s/\s*$//;

        push (@all_lines_separated, $original_code_line) if $original_code_line !~ /^\s*$/; # Push any remaining code that still exists

        $original_code_line = "";
    }
    else {  
        push(@all_lines_separated, $line);
    }
}

# After all of the code is rearranged into a good line, then start doing translation
for my $line (@all_lines_separated){
    
    if ($line !~ /^\s*$/){
        $line_number++; # Increment the line number 
    }

    print "$line_number :\t$line\n" if $debug eq "yes" && $show_line_number eq "yes"; # For Debugging
    print "$line\n" if $debug eq "yes" && $show_line_number eq "no"; # For Debugging

    chomp $line; # Remove any trailing new line
    $line =~ s/^\s*//; # Remove any starting whitespace
    $line =~ s/\s*$//; # Remove any trailing whitespace
    $line =~ s/([^\\])\;$/$1/; # Remove ending semi-colon

    if ($line =~ /^#!/ && $line_number == 1){ # If it is the first line and the line is a shell shebang, go to next in loop
    	next;
    } 
    elsif ($line =~ /^#/){
        # If a line is comment, just print it as a comment
        printHelper("$line\n") if $turn_off_comment_line eq "no";
    }
    elsif ($line =~ /^echo[\s].*/ || $line =~ /^echo$/) { 
        # If echo is found in the line, check if it is executable, then translate
    	handle_echo($line);
    }
    elsif ($line =~ /^[^ ]+\=.+$/) {
        # If an equal is found in a line and matches the regex (no space between the equal sign) then it is a variable that needs to be translated
    	handle_variable_declaration($line);
    }
    elsif ($line =~ /^cd[\s].*/ || $line =~ /^cd$/) { 
        # If cd (change directory) is found in the line, check if it is executable, then translate
        handle_change_directory($line);
    }
    elsif ($line =~ /^mv[\s][^-].*/ || $line =~ /^mv$/) { 
        # If mv (move file) is found in the line and does not have an option then translate
        handle_move_file($line);
    }
    elsif ($line =~ /^chmod[\s][^-].*/ || $line =~ /^chmod$/) {
        # If chmod (change permission) is found in the line and does not have an option then translate
    	handle_chmod($line);
    }
    elsif ($line =~ /^ls[\s][^-].*/ || $line =~ /^ls$/) {
        # If ls (list directory) is found in the line and does not have an option then translate
    	handle_list_file($line);	
    }
    elsif ($line =~ /^rm[\s][^-].*/ || $line =~ /^rm$/) {
        # If rm (Remove file) is found in the line and does not have an option then translate
    	handle_remove($line);
    }
    elsif ($line =~ /^for[\s].+/){ 
        # If for loop is found in the line, then translate the for loop as a python code
        translate_loop($line, "yes");
    }
    elsif ($line =~ /^while[\s].+/){
        # If while loop is found in the line, then translate the for loop as a python code
        handle_if_while_test($line, "while");
    }
    elsif ($line =~ /^exit/  || $line =~ /^exit$/){
        # If exit is found in the line, then translate it to python code
        handle_exit($line);
    }
    elsif ($line =~ /^read/  || $line =~ /^read$/){
         # If read is found in the line, then translate it to python code
        handle_read($line);
    }
    elsif ($line =~ /^[\s]*done[\s]*$/){ 
        # If there is done (from loop), then decrease indentation level
        $indentation-- if $indentation > 0; # Check if indentation is above 0
    }
    elsif ($line =~ /^[\s]*do[\s]*/){   
        # If do is found, then increase the indentation
        $indentation++;
    }
    elsif ($line =~ /^if[\s].+/){ 
        # If there is IF statement, then format it into python code, then add indentation level
        handle_if_while_test($line, "if"); # Translate IF statement
    }
    elsif ($line =~ /^then$/){ 
        # If there is then (from IF), then add indentation level
        $indentation++;
    }
    elsif ($line =~ /^elif[\s].+/){ 
        # Substract the indentation level so that the elif can be in the same indentation as the If statement
        $indentation-- if $indentation > 0;
        handle_if_while_test($line, "elif"); # Translate ELIF statement
    }
    elsif ($line =~ /^else$/){
        $indentation-- if $indentation > 0; # Subtract the indentation level to match the if statement
        printHelper("else\:\n");
        $indentation++; # Increase anything that is after if statement
    }
    elsif ($line =~ /^fi$/){ 
        # If there is fi (from IF), then substract indentation level
        $indentation-- if $indentation > 0; # Check if indentation is above 0
    }
    elsif ($line eq "" || length($line) == 0){ 
        # If it is empty string then should not print anything
        printHelper("\n");
    }
    else { # Anything that can't be translated will be translated as a python subprocess
    	my $translation = translate_shell_builtins($line); # Translate the line that cannot be translated as a subprocess in python

    	if ($translation eq $line){
        	# If lines can't be translated, it is turned into comments
        	printToComment($line) if $turn_off_comment_line eq "no";
    	}
    	else{
    		printHelper("$translation\n"); # Translated line will be printed
    	}
    }
}

# When the program finish reading all the lines of the code, it will print all of the python translated code
finalPrint();





## All of the Handle Functions ##

# Handle Echo Statement
sub handle_echo{
    my $line = $_[0];
    $line =~ /\Q$line\E/; # Escape any metacharacter in the line variable

    my $no_new_line = "no";

    my $new_line = $line;
    $new_line =~ s/^echo//g; 

    if ($line eq $new_line){ # If it cannot be translated, then print as a comment
        printToComment($line);
        return;
    }
    else{        
        $line = $new_line;
        $line =~ s/^\s+//;
        $line =~ s/\s+$//;

        if ($line =~ /^\s*$/){
            printHelper("print;\n"); #if only echo is passed in
            return;
        }

        if ($line =~ /^-n[\s]+/){ # If a -n option is found, notify that the it cannot print new line
            updateImportStatement("sys"); # Add "import sys" code
            $no_new_line = "yes"; # Notify that there can be new line when doing a print
            $line =~ s/-n//;
            $line =~ s/^\s+//;
            $line =~ s/\s+$//;
        }

        my @data = help_split_into_components($line, "yes");
        my $comment_on_line = "";
        my $variable_value = "";

        if (@data > 0){
            $comment_on_line = pop @data if $data[$#data] =~ /^#/; # If there is a comment in a line, then put it inside a variable
        }

        (my $output_filename, my $file_type, @data) = handle_output_command(@data); # Get any output filename and new data if there is an output command

        foreach my $data_x (@data){
            $variable_value .= $data_x;
        }

        if ($variable_value =~ /^\s*$/){
            printToComment($_[0]);
            return;
        }

        my @variable_values = help_split_into_components($variable_value, "yes"); # Help to split the line into components
        my $format_print = help_format_print("yes", @variable_values); # Help print the string format for echo


        if ($output_filename ne ""){ # If the output filename is not empty then it needs to perform and output
            if (defined $file_types{$file_type}){ # Check if the output command is valid
                $file_type = $file_types{$file_type};
                printHelper("with open\($output_filename\, " . $file_type . "\) as f\: print \>\>f\, " . $format_print . "\; " . $comment_on_line . "\n"); # Create a command that will perform an output after a print is executed
            }
            else{ # If the output command is invalid, print it as comment and do a return
		        printToComment($line);
		        return;
            }
        }
        else {
            printHelper("print " . $format_print . "; " . $comment_on_line . "\n") if $no_new_line eq "no"; # If the print requires new line, then use a normal print command
            printHelper("sys.stdout.write (" . $format_print . "\)\; " . $comment_on_line . "\n") if $no_new_line eq "yes"; # If the print does not require new line, then use stdout write in python
        }
    }
}





# Handle Variable Declaration
sub handle_variable_declaration{
    my $line = $_[0];

    my @data = help_split_into_components($line, "yes");
    my $comment_on_line = "";

    if (@data > 0){
        $comment_on_line = pop @data if $data[$#data] =~ /^#/; # Get the comment in a line if it exists
    }

    my $variable_name = "";
    my $variable_value = "";
    my $equal_found = "no";

    foreach my $data_x (@data){
        if ($data_x =~ /^\=$/){ # Check if an equal sign is found, if it is then start recording the variable value
            $equal_found = "yes";
            next;
        }

        if ($equal_found eq "no"){ # If it has yet to find the equal sign, it will record the variable name
            $variable_name .= $data_x;
        }
        elsif ($equal_found eq "yes"){ # If it found the equal sign, it will record the variable value
            $variable_value .= $data_x;
        }
    }

    if ($variable_name eq "" || $variable_value eq ""){ # If the variable value or variable name is empty, print as a comment
        printToComment($line);
        return;
    }

    my @variable_values = help_split_into_components($variable_value, "yes"); # Split the variable value into components
    $variable_value = help_format_print("yes", @variable_values); # Format the variable value components into a single string

    printHelper("$variable_name \= $variable_value; " . $comment_on_line . "\n"); # Print the variable declaration with comment if any
}





# Handle Subprocess Command
sub handle_subprocess{
    updateImportStatement("subprocess"); # Add "import subprocess"

    my $line = $_[0];
    my @splitter = split(" ", $line);
    my $subprocess = "";

    if (@splitter == 0){ # If the shell-builtin cannot be separated into multiple part, print it as a simple subprocess command
        $subprocess = "subprocess.call([\'$line\']);\n";
    }
    else{
        my @data = help_split_into_components($line, "yes");
        my $comment_on_line = "";
        my $command = "";
        my $num_argslists = 0;

        if (@data > 0){
            $comment_on_line = pop @data if $data[$#data] =~ /^#/;
        }

	    (my $output_filename, my $file_type, @data) = handle_output_command(@data);

        foreach my $data_x (@data){
            if (not $data_x =~ /^\s+$/){

                if ($data_x =~ /(\"\$\@\"|\$\@)/){
                    $data_x =~ s/\"\$\@\"//;
                    $data_x =~ s/\$\@//;
                    $num_argslists++;
                    next;
                }

                $data_x =~ s/^\"//g;
                $data_x =~ s/\"$//g;

                $data_x = help_translate_variable($data_x, "no");
                $command .= $data_x . ", ";
            }
        }

        $command =~ s/\, $//;
        $subprocess = "subprocess.call([" . $command . "])"; # Print as formatted   
    
        while ($num_argslists != 0) {
            updateImportStatement("sys");
            $subprocess =~ s/\)$/\+sys\.argv\[1\:\]\)/;
            $num_argslists--;
        }

        if ($output_filename ne ""){ # If there is a output filename then create a python command that perform the shell builtin and output it into the file
            if (defined $file_types{$file_type}){ 

                $file_type = $file_types{$file_type};
                $subprocess =~ s/^subprocess\.call/subprocess\.Popen/;
                $subprocess =~ s/\)$//;

                $subprocess = "with open\($output_filename\, " . $file_type . "\) as f\:" . 
                $subprocess . "\, stderr\=f, stdout\=f\)\.communicate\(\)\[0\]" . "\; " . $comment_on_line; # The python subprocess command that will do an output to a file
            	
            }
            else{
            	$subprocess .= " " .$comment_on_line;
            }
        }
        else{
        	$subprocess .= "\; " .$comment_on_line;
        }
    }

	return $subprocess;
}





# Handle Change Directory Command
sub handle_change_directory{
    my $line = $_[0];
    my $new_line = $line;
    $new_line =~ s/^cd//;

    if ($line eq $new_line){
        printToComment($line);
        return;
    }
    else{
        updateImportStatement("os"); # Update import statement

        $new_line =~ s/^\s+//; # Remove unnecessary starting spaces/tabs
        $new_line =~ s/\s+$//; # Remove unnecessary trailing spaces/tabs

        my @data = help_split_into_components($new_line, "yes");
        my $command = "";
        my $path = "";
        my $comment_on_line = "";

        if (@data > 0){
	        if (@data > 0){
	            $comment_on_line = pop @data if $data[$#data] =~ /^#/;
	        }

            $path = "";
            $path = $data[0] if @data > 0;

            if (not ($path =~ /^\".*\"/ || $path =~ /^\'.*\'/)){
                $path = "\'" . $path . "\'";
            }

            $command = "os.chdir(" . $path . "); " . $comment_on_line;
        }
        else{
            $command = "os.chdir(" . $path . "); " . $comment_on_line;
        }

        printHelper("$command\n");
    }
}





# Handle Move File(s)
sub handle_move_file{
    my $line = $_[0];
    my $new_line = $line;
    $new_line =~ s/^mv//;
    $new_line =~ s/^\s+//; # Remove unnecessary starting spaces/tabs
    $new_line =~ s/\s+$//; # Remove unnecessary trailing spaces/tabs

    if ($line eq $new_line || $new_line eq ""){
        printToComment($line);
        return;
    }
    else{
        updateImportStatement("shutil"); # Add "import shutil"

        my $comment_on_line = "";
        my $for_loop = "for file in"; # Create a loop command that is needed for move file command
        my @data = help_split_into_components($new_line, "yes");

        if (@data > 0){
            $comment_on_line = pop @data if $data[$#data] =~ /^#/;
        }

        if (@data > 1){ # If there is more than 1 data then proceed
            my $destination = pop @data;
            $destination = help_translate_variable($destination, "no");

	        for my $source (@data){
	        	$for_loop .= " " . $source;
	        }

	        $for_loop = translate_loop($for_loop, "no");

	        printHelper($for_loop);
	        printHelper("\t" . "shutil.move(file\, " . $destination . ")\; " . $comment_on_line . "\n");
    	}
    	else{
    		printToComment($line);
    	}
    }
}





# Handle Change Permission of File(s)
sub handle_chmod{
    my $line = $_[0];
    my $new_line = $line;
    $new_line =~ s/^chmod//;
    $new_line =~ s/^\s+//; # Remove unnecessary starting spaces/tabs
    $new_line =~ s/\s+$//; # Remove unnecessary trailing spaces/tabs

    if ($line eq $new_line || $new_line eq ""){
        printToComment($line);
        return;
    }
    else{
    	updateImportStatement("os");

        my $permission_code = "";
        my $for_loop = "for file in";

        my @data = help_split_into_components($new_line, "yes");

        if (@data > 1){ # If there is more than 1 data then proceed
	        for my $data_x (@data){
	        	if ($permission_code eq "" && $data_x !~ /^\s*$/){
	        		$permission_code = $data_x;
	        		$permission_code = sprintf "%04s", $permission_code;
	        		next;
	        	}
	        	else{
	        		$for_loop .= " " . $data_x;
	        	}
	        }

	        $for_loop = translate_loop($for_loop, "no");
	        printHelper($for_loop);

	        printHelper("\t" . "os.chmod(file\, " . $permission_code . ")\;\n");
	    }
	    else{
	    	printToComment($line);
	    }
    }
}





# Handle List File(s) in Directory
sub handle_list_file{
    my $line = $_[0];
    my $new_line = $line;
    $new_line =~ s/^ls//;
    $new_line =~ s/^\s+//; # Remove unnecessary starting spaces/tabs
    $new_line =~ s/\s+$//; # Remove unnecessary trailing spaces/tabs

    if ($line eq $new_line){
        printToComment($line);
        return;
    }
    else{
    	updateImportStatement("os");

        my @data = help_split_into_components($new_line, "yes");
        my $comment_on_line = "";
        my $for_loop = "for file in";

        my $count_file = 0;

	    (my $output_filename, my $file_type, @data) = handle_output_command(@data);

        if (@data > 0){
            $comment_on_line = pop @data if $data[$#data] =~ /^#/;
        }

        if (@data == 0){
            push(@data, "./");
        }

 		
 		for my $data_x (@data){
 			if ($data_x !~ /^\s*$/){		
	 			$data_x =~ s/^[\"\']//g;
	 			$data_x =~ s/[\"\']$//g;
 				$for_loop .= " " . $data_x;
 				$count_file++; # Increase the count of files
 			}
 		}

        $for_loop = translate_loop($for_loop, "no");
        chomp($for_loop);
        printHelper($for_loop . $comment_on_line . "\n");


        if ($output_filename ne ""){

			if (defined $file_types{$file_type}){
				if ($file_type eq ">"){
					# Hacky way (Though simpler)
					unlink "$output_filename";
					$file_type = ">>";
				}

                $file_type = $file_types{$file_type};
                printHelper("\t" . "with open\($output_filename\, " . $file_type . "\) as f\: print \>\>f\, file, \"\:\"\;\n") if $count_file > 1;
    	        printHelper("\t" . "for filename in sorted(os.listdir(file))\:\n");

    	        printHelper("\t\t" . "if not filename.startswith(\'\.\')\:\n");
                printHelper("\t\t\t" . "with open\($output_filename\, " . $file_type . "\) as f\: print \>\>f\, filename\;\n");
                printHelper("\t" . "with open\($output_filename\, " . $file_type . "\) as f\: print \>\>f\, \"\"\;\n") if $count_file > 1;
            }
            else{
		        printToComment($line);
		        return;
            }
    	}
        else {
	        printHelper("\t" . "print file, \"\:\"\;\n") if $count_file > 1;
	        printHelper("\t" . "for filename in sorted(os.listdir(file))\:\n");

	        printHelper("\t\t" . "if not filename.startswith(\'\.\')\:\n");
	        printHelper("\t\t\t" . "print filename\;\n");
	        printHelper("\t" . "print \"\"\;\n") if $count_file > 1;
    	}
    }
}





# Handle Remove File(s)
sub handle_remove{
    my $line = $_[0];
    my $new_line = $line;
    $new_line =~ s/^rm//;
    $new_line =~ s/^\s+//; # Remove unnecessary starting spaces/tabs
    $new_line =~ s/\s+$//; # Remove unnecessary trailing spaces/tabs

    if ($line eq $new_line){
        printToComment($line);
        return;
    }
    else{
    	updateImportStatement("os"); # Update import statement

        my $comment_on_line = "";
        my $for_loop = "for file in";
        my @data = help_split_into_components($new_line, "yes");

        if (@data > 0){
            $comment_on_line = pop @data if $data[$#data] =~ /^#/;
        }

        if (@data > 0){ # If there is more than 1 data then proceed            
            for my $source (@data){
                $for_loop .= " " . $source;
            }

            $for_loop = translate_loop($for_loop, "no");

            printHelper($for_loop);
            printHelper("\t" . "os.remove(file)\; " . $comment_on_line . "\n");
        }
        else{
            printToComment($line);
        }
	}
}





# Handle Exit Command
sub handle_exit{
    my $line = $_[0];
    my $new_line = $line;
    $new_line =~ s/^exit//;

    if ($line eq $new_line){
        printToComment($line);
        return;
    }
    else{
        updateImportStatement("sys");

        $new_line =~ s/^\s+//; # Remove unnecessary starting spaces/tabs
        $new_line =~ s/\s+$//; # Remove unnecessary trailing spaces/tabs

        my @data = help_split_into_components($new_line, "yes");
        my $command = "";
        my $exit_code = "";
        my $comment_on_line = "";

        if (@data > 0){
	        if (@data > 0){
	            $comment_on_line = pop @data if $data[$#data] =~ /^#/;
	        }

            $exit_code = "";
            $exit_code = $data[0] if @data > 0 && $data[0] =~ /^[\"\']?[\d]+[\"\']?$/;

            if ($exit_code =~ /^\".*\"/ || $exit_code =~ /^\'.*\'/){
                $exit_code =~ s/\"//g;
                $exit_code =~ s/\'//g;
            }

            $command = "sys.exit(" . $exit_code . "); " . $comment_on_line;
        }
        else{
            $command = "sys.exit(" . $exit_code . "); " . $comment_on_line;
        }

        printHelper("$command\n");
    }
}





# Handle Read Command
sub handle_read{
    my $line = $_[0];
    my $new_line = $line;
    $new_line =~ s/^read//;

    if ($line eq $new_line){
        printToComment($line);
        return;
    }
    else{
        updateImportStatement("sys");

        $new_line =~ s/^\s+//; # Remove unnecessary starting spaces/tabs
        $new_line =~ s/\s+$//; # Remove unnecessary trailing spaces/tabs

        my @data = help_split_into_components($new_line, "yes");
        my $command = "";
        my $comment_on_line = "";

        if (@data > 0){
	        if (@data > 0){
	            $comment_on_line = pop @data if $data[$#data] =~ /^#/;
	        }

            if (@data > 0){
                foreach my $variable (@data){
                    if ($variable !~ /^\s*$/){
                        $command = "$variable = sys.stdin.readline().rstrip(); " . $comment_on_line;
                        printHelper("$command\n");
                    }
                }
            }
            else{
                $command = "sys.stdin.readline().rstrip(); " . $comment_on_line;
                printHelper("$command\n"); 
            }
        }
        else{
            $command = "sys.stdin.readline().rstrip(); " . $comment_on_line;
            printHelper("$command\n");
        }
    }
}





# Handle For loop statement
sub handle_for_loop{
    my $line = $_[0];
    my $new_line = $line;
    my $for_line = "";

    $new_line =~ s/^for//;

    my $command = "";

    if ($line eq $new_line){
        printToComment($line);
    }
    else{
        $new_line =~ s/^\s+//; # Remove unnecessary starting spaces/tabs
        $new_line =~ s/\s+$//; # Remove unnecessary trailing spaces/tabs

        my @data = help_split_into_components($new_line, "yes");
        my $command = "";
        my $comment_on_line = "";

        if (@data > 0){
            $comment_on_line = pop @data if $data[$#data] =~ /^#/;
        }

        if (@data == 0){
            printToComment($line);
        }
        else{
        	my $for_variable = "";
        	my $in_found = "no";
        	my $all_variable_value = "";
        	my @variable_values = ();

        	for my $data_x (@data){
                if ($data_x !~ /^\s*$/ && $for_variable eq ""){
                	$for_variable = $data_x;
                	next;
                }
                elsif ($data_x !~ /^\s*$/ && $in_found eq "no" && $data_x =~ /^in$/){
                	$in_found = "yes";
                	next;
                }
                elsif ($data_x !~ /^\s*$/ && $in_found eq "yes"){
                	
                	if ($data_x =~ /^\`.*\`$/){
                		$data_x = translate_backticks($data_x);
                		$data_x = "[" . $data_x . "]";
                	}
                	else{	
                		my $new_data = $data_x;
                		my $quote_type = "";

			        	if ($new_data =~ /^(\"|\')/ && $new_data =~ /(\"|\')$/){
			        		$quote_type = $1;

	    		        	$new_data =~ s/^[\"\']//;
				        	$new_data =~ s/[\"\']$//;
			        	}

                		my @file_array = glob "$new_data" ; # Get number of files that matches pattern

                		if ($new_data !~ /\\/ && @file_array > 1 || (@file_array == 1 && $file_array[0] ne $new_data)){ # If any file matches, use glob otherwise its a string
                			updateImportStatement("glob");

                			$data_x = "sorted(glob.glob(\"$new_data\"))";
                			$data_x = "[" . "\' \'.join(sorted(glob.glob(\"$new_data\")))" . "]" if $quote_type =~ /^[\"\']$/;
                		}
                		else{
	                		my $converted = help_translate_variable($data_x, "no");

                            if ($converted =~ /^sys\.argv\[1\:\]$/){
                		         $data_x = $converted;
                            }
                            else{
                                $data_x = "[" . $converted . "]";
                            }
                		}
                	}

                	push(@variable_values, $data_x);
                	next;
                }
        	}

        	$all_variable_value = join(" + ", @variable_values);

            $command = "for $for_variable in $all_variable_value\: " . $comment_on_line;
            $for_line = "$command\n";
        }
    }

    return $for_line;
}





# Handle Special Variable Declaration
sub handle_special_variable {
    my $line = $_[0];
    my $argument_number = "";

    if ($line =~ /^\s*\$(\d)+\s*$/ || $line =~ /^\s*\"\$(\d)+\"\s*$/){
        updateImportStatement("sys");
        $argument_number = $1;

        return "str(sys.argv[$argument_number])";
    }
    elsif ($line =~ /^\s*\$(\@|\*)\s*$/ || $line =~ /^\s*\"\$(\@|\*)\"\s*$/){
    	#Convert from lists into a one line string
        updateImportStatement("sys");
        return "sys.argv[1\:]";
    }
    elsif ($line =~ /^\s*\$(\#)\s*$/ || $line =~ /^\s*\"\$(\#)\"\s*$/){
        updateImportStatement("sys");
        return "len(sys.argv[1\:])";
    }
    elsif ($line =~ /^\s*\$(\$)\s*$/ || $line =~ /^\s*\"\$(\$)\"\s*$/){
        updateImportStatement("os");
        return "str(os.getpid())";
    }
    elsif ($line =~ /^\s*\$([A-Za-z0-9_]+)\s*$/ || $line =~ /^\s*\"\$([A-Za-z0-9_]+)\"\s*$/){
    	my $variable_name = $1;
        return "str($variable_name)";
    }
    else{
        return $line;
    }
}





# Handle If and While loop statement
sub handle_if_while_test{
    my ($line, $prefix) = @_;

    my $new_line = $line;
    $new_line =~ s/^$prefix// if $new_line =~ /^$prefix/;

	$new_line =~ s/^\s+//; # Remove unnecessary starting spaces/tabs
    $new_line =~ s/\s+$//; # Remove unnecessary trailing spaces/tabs

    $new_line =~ s/^\`//; # Remove unnecessary starting spaces/tabs
    $new_line =~ s/\`$//; # Remove unnecessary trailing spaces/tabs

    if ($new_line =~ /^test (.+)$/ || $new_line =~ /^\[\[ (.+) \]\]$/ || $new_line =~ /^\[ (.+) \]/){
    	$new_line = $1;

    	if ($new_line =~ /\] \&\& \[/){
    		$new_line =~ s/\] (\&\&) \[/$1/g;
    	}

    	if ($new_line =~ /\] \|\| \[/){
    		$new_line =~ s/\] (\|\|) \[/$1/g;
    	}

        my $not_if_while = " ";
        my $condition = "";
        my $comment_on_line = "";

        if ($new_line =~ /^\s*![^\=]/){
            $not_if_while = " not ";
            $new_line =~ s/^\s*![^\=]\s*//;
        }

        my @data = help_split_into_components($new_line, "yes");

        if (@data > 0){
            $comment_on_line = pop @data if $data[$#data] =~ /^#/;
        }

        if (@data == 0){
            printToComment($line);
            return;
        }
        else{
	        if ($new_line =~ /\s+(\=|\!\=|\=\=|\-le|\-lt|\-ge|\-gt|\-eq|\-ne|\-o|\-a)\s+/){
	            my $is_number_comparison = "yes";
                my $last_character = "";

	            if ($new_line =~ /\s+(\=|\!\=|\=\=)\s+/){
	            	$is_number_comparison = "no";
	            }

	            foreach my $data_x (@data){
		            my $converted = $data_x;
                    $converted =~ s/^\"//;
                    $converted =~ s/\"$//;

		            if ($converted !~ /^\s*$/){
			            if (translate_operator($converted) ne $converted){

                            if ($last_character =~ /[\=\!]/ && $converted eq "\="){
                                $converted = "";
                            }
			                else{
                                $converted = translate_operator($converted);
                            }

                        }
			            else{
							my @file_array = glob "$data_x" ; # Get number of files that matches pattern

				    		if ($data_x !~ /^[\"\'].*[\"\']$/ && $data_x !~ /\\/ && (@file_array > 1 || (@file_array == 1 && $file_array[0] ne $data_x))){ # If any file matches, use glob otherwise its a string
				    			updateImportStatement("glob");
				    			$converted = "\' \'\.join(sorted(glob.glob(\"$data_x\")))";
				    		}
				    		else{
					            $converted = help_translate_variable($data_x, "no");
				        	}
		            	}
		        	}

                    if ($converted =~ /^\"(\d)+\"$/){
                        $converted =~ s/^\"//;
                        $converted =~ s/\"$//;
                        $is_number_comparison = "yes"
                    }

	                $condition .= $converted;
                    $last_character = $data_x;
	            }

	            if ($is_number_comparison eq "yes"){
	    	        # a combination of alphanumeric
                	$condition =~ s/str\((.*?)\)/int\($1\)/g; # Convert to integer if and only if it is not
	            }
        	}
        	elsif ($new_line =~ /^\s*\-(O|G|r|w|x|e|a|s|f|d|h)/){ # Check if the IF statement tests for file readability 
                my $option = "";
	            my $document = "";

	            foreach my $data_x (@data){
	            	if ($data_x !~ /^\s*$/ && $option eq ""){
	            		$option = $data_x;
	            	}
	            	elsif ($data_x !~ /^\s*$/ && $document eq ""){
	            		$document = $data_x;
	            		$document =~ s/^\"//;
	            		$document =~ s/\"$//;

			            $document = help_translate_variable($document, "no");
			            last;
	            	}
	            }

	            if ($option eq "-O"){
	            	$condition = "os\.stat\(" . $document . "\)\.st_uid \=\= os\.geteuid\(\)";
	            }
	            elsif ($option eq "-G"){
	            	$condition = "os\.stat\(" . $document . "\)\.st_gid \=\= os\.getegid\(\)";
	            }
	            elsif ($option eq "-r"){
	            	$condition = "os\.access\(" . $document . "\, os\.R\_OK)";
	            }
	            elsif ($option eq "-w"){
	            	$condition = "os\.access\(" . $document . "\, os\.W\_OK)";
	            }
				elsif ($option eq "-x"){
	            	$condition = "os\.access\(" . $document . "\, os\.X\_OK)";
	            }
	            elsif ($option eq "-e" || $option eq "-a"){
	            	$condition = "os\.path\.exists\(" . $document . "\)";
	            }
	            elsif ($option eq "-s"){
	            	$condition = "os\.path\.getsize\(" . $document . "\) \> 0";
	            }
	            elsif ($option eq "-f"){
	            	$condition = "os\.path\.isfile\(" . $document . "\)";
	            }
	            elsif ($option eq "-d"){
	            	$condition = "os\.path\.isdir\(" . $document . "\)";
	            }
	            elsif ($option eq "-h"){
	            	$condition = "os\.path\.islink\(" . $document . "\)";
	            }
	            else{
            		printToComment($line);
            		return;
	            }

	            if ($condition =~ /^os\.path/){
	        		updateImportStatement("os.path");
	            }
	            else{
	        		updateImportStatement("os");
	            }
			}
            elsif ($not_if_while ne ""){
                $condition = help_translate_variable($new_line, "no");
            }
        	else{
            	printToComment($line);
            	return;
        	}
        }

        printHelper("$prefix" . "$not_if_while" .  "$condition: $comment_on_line\n");
    }
    elsif ($new_line =~ /^test[^\s]/ || $new_line =~ /^\[\[[^\s]/ || $new_line =~ /^\[[^\s]/){
        printToComment($line);
    }
    else{
		$new_line =~ s/^\s+//; # Remove unnecessary starting spaces/tabs
        $new_line =~ s/\s+$//; # Remove unnecessary trailing spaces/tabs

        my $translation = translate_shell_builtins($new_line);
        $translation =~ s/\;\s*$//; # Remove ending semi-colon, cause it is in while loop or if loop

	    printHelper("$prefix not $translation\:\n");
    }
}





## All of the Helper Functions ##

# Help to Split a line into appropriate components ( MAIN HELPER FUNCTION ~ HELPFUL )
# Split into components such as Double-quotes, Single-quotes, Backticks, Space-Separated Text and Hashtag (Comment)
sub help_split_into_components{
    my $line = $_[0];
    my $start_process_data = $_[1];

    my @split_letter = split('', $line);
    my @data = ();
    my $in_single_quote = "no";
    my $in_double_quote = "no";
    my $in_backtick = "no"; # need to start doing
    my $in_hash = "no"; # need to start doing

    my $text = "";
    my $last_character = "";

    foreach my $splitted (@split_letter){
        if ($splitted =~ '^\"$' && $in_double_quote eq "no" && $in_single_quote eq "no" && $in_backtick eq "no" && $in_hash eq "no" && not $last_character eq "\\"){
            if (not $text eq ""){
                push(@data, $text);
                $text = "";
            }

            $in_double_quote = "yes";
            $text .= "\"";
        }
        elsif ($splitted =~ '^\'$' && $in_double_quote eq "no" && $in_single_quote eq "no" && $in_backtick eq "no" && $in_hash eq "no" && not $last_character eq "\\"){
            if (not $text eq ""){
                push(@data, $text);
                $text = "";
            }

            $in_single_quote = "yes";
            $text .= "\'";
        }
        elsif ($splitted =~ '^\`$' && $in_double_quote eq "no" && $in_single_quote eq "no" && $in_backtick eq "no" && $in_hash eq "no" && not $last_character eq "\\"){
            if (not $text eq ""){
                push(@data, $text);
                $text = "";
            }

            $in_backtick = "yes";
            $text .= "\`";
        }
        elsif ($splitted =~ '^\#$' && $in_double_quote eq "no" && $in_single_quote eq "no" && $in_backtick eq "no" && $in_hash eq "no" && not ($last_character eq "\\" || $last_character eq "\$")){
            if (not $text eq ""){
                push(@data, $text);
                $text = "";
            }

            $in_hash = "yes";
            $text .= "\#";
        }
        elsif ($splitted =~ '^(\=)$' && $in_double_quote eq "no" && $in_single_quote eq "no" && $in_backtick eq "no" && $in_hash eq "no" && not $last_character eq "\\"){
            my $metacharacter = $1;

            if (not $text eq ""){
                push(@data, $text);
                $text = "";
            }

            push(@data, $metacharacter);
        }



        elsif ($splitted =~ '^\"$' && $in_double_quote eq "yes" && $in_single_quote eq "no" && $in_backtick eq "no" && $in_hash eq "no" && not $last_character eq "\\"){
            $in_double_quote = "no";
            $text .= "\"";

            if (not $text eq ""){
                print "Double Quote found : $text\n" if $debug eq "yes" && $start_process_data eq "yes";
                push(@data, $text);
                $text = "";
            }
        }
        elsif ($splitted =~ '^\'$' && $in_double_quote eq "no" && $in_single_quote eq "yes" && $in_backtick eq "no" && $in_hash eq "no" && not $last_character eq "\\"){
            $in_single_quote = "no";
            $text .= "\'";

            if (not $text eq ""){
                print "Single Quote found : $text\n" if $debug eq "yes" && $start_process_data eq "yes";
                push(@data, $text);
                $text = "";
            }
        }
        elsif ($splitted =~ '^\`$' && $in_double_quote eq "no" && $in_single_quote eq "no" && $in_backtick eq "yes" && $in_hash eq "no" && not $last_character eq "\\"){
            $in_backtick = "no";
            $text .= "\`";

            if (not $text eq ""){
                print "Backtick found : $text\n" if $debug eq "yes" && $start_process_data eq "yes";
                push(@data, $text);
                $text = "";
            }
        }



        elsif ($splitted =~ /^\s$/ && $in_double_quote eq "no" && $in_single_quote eq "no" && $in_backtick eq "no" && $in_hash eq "no"){
            if (not $text eq ""){
                push(@data, $text);
            }

            push(@data, " ");
            $text = "";
        }


        elsif ($in_double_quote eq "yes" && $in_single_quote eq "no" && $in_backtick eq "no"){        	
        	if ($splitted =~ /^(\')$/ && not $last_character eq "\\"){
        		$splitted = "\\" . $splitted;
        	}

        	$text .= "$splitted";
        }
        elsif ($in_double_quote eq "no" && $in_single_quote eq "yes" && $in_backtick eq "no"){
        	if ($splitted =~ /^(\")$/ && not $last_character eq "\\"){
        		$splitted = "\\" . $splitted;
        	}

            $text .= "$splitted";
        }
        else{
            $text .= "$splitted";
        }

        $last_character = $splitted;
    }

    if (not $text eq ""){
        $in_double_quote = "no";
        $in_single_quote = "no";
        $in_backtick = "no";
        $in_hash = "no";

        push(@data, $text) if not $text eq "";
    	$text = "";
    }

    return @data;
}





# Help to Format Data components passed in into a string format
sub help_format_print{
    my($convert_variable, @data) = @_;
    my $return_line = "";

    my @print_format = ();
    my @print_value = ();

    foreach my $data_x (@data){
        my @variables = ();

        if ($data_x =~ /^\'.*\'$/){ # If a component is a single-quote, no need to translate
            push(@print_format, "%s"); # Push the format
            push(@print_value, $data_x); # Push the value
        }
        elsif ($data_x =~ /^\`.*\`$/){ # If a component is a backtick, translate it
        	my $translation = translate_backticks($data_x); # Translate the backtick command
	        push(@print_format, "%s"); # Push the format
            push(@print_value, $translation); # Push the value
        }
        else{ # If a component is a double quote or not in quote, translate it
        	my $new_data = $data_x;

        	$new_data =~ s/^\"//;
        	$new_data =~ s/\"$//;

			my @file_array = glob "$new_data" ; # Get number of files that matches pattern

    		if ($data_x !~ /^\".*\"$/ && $data_x !~ /\\/ && (@file_array > 1 || (@file_array == 1 && $file_array[0] ne $new_data))){ # If any file matches, use glob otherwise its a string
    			
                updateImportStatement("glob"); # Add "import glob"
    			$data_x = "\' \'\.join(sorted(glob.glob(\"$new_data\")))";

	            push(@print_format, "%s");
	            push(@print_value, $data_x);

    		}
    		else{
	            @variables = ($data_x =~ /(.?\$[A-Za-z0-9_]+|.?\$[\@\#\*\$])/g);
            	#$new_data = $data_x;

	            for my $variable (@variables){
	                my $temp_variable = $variable;
	                $temp_variable =~ s/^\s*|\s*$//;

	                if ($temp_variable =~ /^[^\\]?\$/){
	                    $temp_variable =~ s/^[^\$]?\$//;

	                    $new_data =~ s/\$\Q$temp_variable/\%s/; # Added \Q to escape dollar sign
	                    $new_data =~ s/\\\$/\$/;

	                    $temp_variable = "\$" . $temp_variable;

	                    if ($convert_variable eq "yes"){
	                		$temp_variable = help_translate_variable($temp_variable, "no");
	                    	push(@print_value, $temp_variable);
	                	}
	                	else{
	                		$temp_variable = "\"" . $temp_variable . "\"" if $temp_variable !~ /^[\"\'].*[\"\']$/; # If it does not start with quote (single or double) and ends with quote, then add them.
	                		push(@print_value, $temp_variable); # Push the variable into printable value
	                	}
	                }
	            }

	            push(@print_format, $new_data);
	        }
        }
    }

    if (@data == 1){
        my $joiner_format = "";
        my $joiner_value = "";

        $joiner_format = join("", @print_format);
        $joiner_value = join(", ", @print_value);

        if (not $joiner_format =~ /^\".*\"$/ && not $joiner_format =~ /^\'.*\'$/){
            $joiner_format =~ s/(?<!\\)\"//g;
            $joiner_format =~ s/(?<!\\)\'//g;

            $joiner_format = "\"" . $joiner_format . "\"";
        }

        # Consider
        $joiner_format =~ s/\\/\\\\\\/g if $joiner_format =~ /^\'.+\'$/; # If there is a back
        $joiner_format =~ s/(?:\\{1})?\\\$/\$/g if $joiner_format =~ /^\".+\"$/ || $joiner_format =~ /^\'.+\'$/;


        # Make the code more readable
        if (@print_value == 1 && @print_format == 0){
        	$return_line = $joiner_value;
        }
        elsif (@print_value == 0 && @print_format == 1){
        	$return_line = $joiner_format;
        }
        else{
	        $return_line = $joiner_format;
	        $return_line .= " \% (" . $joiner_value . ")"  if length($joiner_value) > 0;
    	}
    }
    elsif (@data > 1){
        my $joiner_format = join("", @print_format);
        my $joiner_value = join(", ", @print_value);

        if ($joiner_format =~ /^\".*\"$/){
            $joiner_format = "\"" . $joiner_format . "\"";
        }
        elsif (not $joiner_format =~ /^\".*\"$/ && not $joiner_format =~ /^\'.*\'$/){

            $joiner_format =~ s/(?<!\\)\"//g;
            $joiner_format =~ s/(?<!\\)\'//g;

            $joiner_format = "\"" . $joiner_format . "\"";
        }

        # Consider
        $joiner_format =~ s/\\/\\\\\\/g if $joiner_format =~ /^\'.+\'$/; # If there is a back
        $joiner_format =~ s/(?:\\{1})?\\\$/\$/g if $joiner_format =~ /^\".+\"$/ || $joiner_format =~ /^\'.+\'$/;

        # Make the code more readable
        if (@print_value == 1 && @print_format == 0){
        	$return_line = $joiner_value;
        }
        elsif (@print_value == 0 && @print_format == 1){
        	$return_line = $joiner_format;
        }
        else{
	        $return_line = $joiner_format;
	        $return_line .= " \% (" . $joiner_value . ")"  if length($joiner_value) > 0;
    	}
    }

    return $return_line;
}





# Act as a bridge for the calling code and subprocess handler
sub translate_shell_builtins{
	my $line = $_[0];
	my $return_data = handle_subprocess($line); # Translate the unsupported shell-builtin command

    return $return_data; # Rreturn subprocess command containing the shell-builtins
}





# Act as a bridge between the calling code and the for loop handler
sub translate_loop{
	my $line = $_[0];
	my $should_print = $_[1];
	my $return_loop = handle_for_loop($line); # Translate the for loop

	if ($should_print eq "yes"){ # Checks whether the code should be printed straight away
		printHelper("$return_loop");
	}
	else{
		return $return_loop; # Return the translated for loop
	}
}





# Translate backticks information
sub translate_backticks{

    my $backticks = $_[0];
    $backticks =~ s/^\`//; # Remove starting backtick
    $backticks =~ s/\`$//; # Remove ending backtick
    $backticks =~ s/^\s+//g; # Remove any starting whitespace
    $backticks =~ s/\s+$//g; # Remove any trailing whitespace


    # Initialize required variable
    my $formula = "";
    my $num_argslists = 0;


    my @data = help_split_into_components($backticks, "yes"); # Split backtick command into components

    if (@data >= 1){ # Check if the components have at least one file
	    my $backtick_method = $data[0]; # Get the first item in array

	    if ($backtick_method =~ /^expr$/){ # Check if the command is an expr command

		    @data = @data[ 1 .. $#data ]; # Remove the first item in array

	        for my $data_x (@data){
	            if (not $data_x eq ""){ # If the component is not empty, then proceed

                    # Remove any starting and ending quotes (single and double)
	                $data_x =~ s/^\'//;
	                $data_x =~ s/\'$//;
	                $data_x =~ s/^\"//;
	                $data_x =~ s/\"$//;

	                $data_x = help_translate_variable($data_x, "yes"); # Try to translate the data if it is a variable         

	                $data_x =~ s/\\//; # Remove any backticks in an expr command (e.g. \*)

	                $data_x =~ s/^str\(/int\(/; # Converts any str( ) into int ( ) as it is an expr command
	                $data_x =~ s/^[\'\"]/int\(/; # Converts any " or ' into int ( ) as it is an expr command
	                $data_x =~ s/[\'\"]$/\)/; # Add the closing bracket

	                $formula .= $data_x; # Append the code as a formula
	            }
	        }

	        $formula =~ s/^\s*//; # Remove any starting whitespace
	        $formula =~ s/\s*$//; # Remove any trailing whitespace

	    }
	    else{ # If it is not an expr command, use os.popen to perform a backtick command
	        updateImportStatement("os"); # Add "import os"

	        my $format_backticks = help_format_print("yes", @data); # Format the backticks as necessary
	        $formula = "os.popen(" . $format_backticks . ").read().rstrip()"; # Set the formula into an os.popen command with string format (if needed)
	    }
	}

    return $formula; # Return the translated backtick command
}





# Translate all >> and > output command
sub handle_output_command{

    # Initialize necessary variables
    my(@data) = @_;
    my @new_data = ();
    my $file_type = "";
    my $output_filename = "";
    my $start_record_filename = "no";
    
    for my $data_x (@data){
        if ($start_record_filename eq "no" && $data_x =~ /^(>>|>)/){ # Check if it has not started recording the output filename and there is an output command
            $start_record_filename = "yes"; # Set to start recording the filename
            $output_filename .= $data_x; # Apped the filename
        }
        elsif ($start_record_filename eq "yes" && $output_filename =~ /^(>>|>)$/ && $data_x =~ /^[^\s]+$/){
            $output_filename .= $data_x; # Apped the filename
        }
        elsif ($start_record_filename eq "yes" && $output_filename =~ /^(>>|>)$/ && $data_x =~ /^\s+$/){
            next; # Go to next in loop
        }
        elsif ($start_record_filename eq "yes" && $output_filename !~ /^(>>|>)$/ && $data_x =~ /^\s+$/){
            $start_record_filename = ""; # Set to stop record if a space is found
        }
        else{
            push(@new_data, $data_x); # Any other code will be pushed into an array
        }
    }

    if (not $output_filename eq ""){
        $output_filename =~ s/(>>|>)//; # Remove the output file command
        $file_type = $1; # Get the output file command

        $output_filename =~ s/^\s+//g; # Remove any starting whitespace
        $output_filename =~ s/\s+$//g; # Remove any trailing whitespace

        $output_filename = help_translate_variable($output_filename, "no"); # Translate the filename if it is a variable, if not it will return the original filename
    }

    return ($output_filename, $file_type, @new_data); # Return the output filename, the output file command and the new code array
}





# Translate all the comparators in shell into python equivalent comparators
sub translate_operator{
    my $comparator = $_[0]; # The comparator that needs to be translated

    if (defined $expr_ops{$comparator}){ # Check if the comparator is a string comparison
    	return $expr_ops{$comparator};
    }
    elsif (defined $test_seperators{$comparator}){ # Check if the comparator is a test separator
    	return $test_seperators{$comparator};
    }
    elsif (defined $numeric_test{$comparator}){ # Check if the comparator is a numeric operator
    	return $numeric_test{$comparator};
    }
    elsif (defined $file_types{$comparator}){ # Check if the comparator is a file creation
    	return $file_types{$comparator};
    }
    elsif (defined $test_operators{$comparator}){ # Check if the comparator is a test operator
    	return $test_operators{$comparator};
    }

    return $comparator; # Return the original comparator value if it is not converted
}





# Helper method to translate variable (doing this make the code more concise and simpler to manage)
sub help_translate_variable{
	my $variable = $_[0]; # Variable that needs to be translated
	my $no_quote = $_[1]; # Variable to determine whether the variable is quoted

	my $converted_variable = $variable;

	if (not $variable =~ /^\'\$.+\'/){
		$converted_variable = handle_special_variable($variable);

		    if ($converted_variable eq $variable && $no_quote eq "no"){ # Check if the conversion is not successful and if it is not quoted
				$converted_variable = "\"" . $converted_variable . "\""; # Add a quote between the unsuccessful conversion
		}
	}

	return $converted_variable; # Return the converted variable whether it is successful or not
}

 



# Store all of the translated python code into an array before doing a final print
sub printHelper{
    my $to_print = $_[0];
    my $tab = ""; # Used to create the indentation level of a line

    # Add the indentation as required
    for (my $i = 0; $i < $indentation; $i++){
        $tab .= "\t";
    }
    
    push(@code_array, "$tab$to_print"); # Push the code with the level of indentation to the array of code
}





# A Function that helps to comment a line of code if it cannot be translated
sub printToComment{
    my $comment = $_[0];
    printHelper("#$comment\n");
}





# Update the Import statement as necessary
sub updateImportStatement{
    my $import_file = $_[0];

    if (not grep(/^$import_file$/, @import_used)) { # Check whether the import has been used before
        push(@import_used, "$import_file"); # Push into an array if the import has not been used before
    }
}





# Print all of the python translated code (including comment)
sub finalPrint{
	print "#!/usr/bin/python2.7 -u\n"; # Print the python 2.7 statement

    if ($debug eq "no" && $should_do_final_print eq "yes") {

        foreach my $import (sort @import_used){ # Sort and Print all of the import statement alphabetically
            print "import $import\n";
        }

        foreach my $code (@code_array){ # Print all of the translated python code
            print "$code";
        }

    }
}
