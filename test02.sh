#!/bin/sh

#
# cs2041, 15s2
#

#
# This is a test of Level 2 features.
# Test : 2
#

not_print="yes"
will_print="yes"

echo '$not_print'
echo "$will_print"

echo "$1", '$1', $1

pwd; number=3; cd test

option="";
invalid_username=0;
username=""

while [ ! $username ]
do
	echo -n "Enter your Username : "
	read username
	invalid_username=`expr $invalid_username + 1`

done

echo "Invalid username entered : '$invalid_username'";

if [ $username ]; then echo "Welcome, " $username; else echo "Fail to recognize username : "; exit 123; fi


echo "Argument 1 given is $1"

# Annoying code above

if test $1 -eq 1 -o $1 -eq 2
then
	echo "Main Option Selected"
else
	echo "Other Option Selected"
fi

