#!/bin/sh

#
# cs2041, 15s2
#

# Partially taken from cs2041 lab week 3 (echon.sh)
# Test on : If, Test Expr, [ ], Echo, While loop, Expr, Exit
# Demo : 1

if test $# = 2
then
	
	if [ $1 -gt 0 ]
	then

		start=1
		while test $start -le $1
		do
			echo $2
			start=`expr $start + 1`
		done

	else 
		echo "$0: argument 1 must be a non-negative integer"
     	exit 1
	fi

else
	echo "Usage: $0 <number of lines> <string>"
    exit 1
fi
