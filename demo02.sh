#!/bin/sh

#
# cs2041, 15s2
#

# Checks password if it is correct, print every words (or files) together with the current date and put it into notes.txt
# Test on : Variable, While loop, Echo (-n), Read, If, Shell-built-in, Backticks, For loop, >>, Exit
# Demo : 2

password="pass1234";
read_password="";

while [ $read_password != $password ]
do

	echo -n "Enter your password: ";
	read read_password;

	if [ $read_password == $password ]
	then
		echo "Password is correct!";

		date=`date`

		for word in Houston 1202 *.sh
		do
			echo $date " : " $word >> notes.txt # Print the for words and store it into notes.txt
		done

		echo "notes.txt created"

		exit 1
	else
		echo "Please enter a correct password!"
	fi

done