#!/bin/sh

#
# cs2041, 15s2
#

# Simple Arithmetic Calculation using stdin and expr
# Test on : Echo (-n), Variable, While, Read, Case, Bacitck expr
# Demo : 3

echo "Welcome to Printy!";

option="";
first_number="";
second_number="";


while [ ! $option ]
do
	echo -n "Option (1 : Addition, 2 : Subtraction, 3 : Multiplication, 4 : Division) : "
	read option
done


while [ ! $first_number ]
do
	echo -n "1st Number : "
	read first_number
done


while [ ! $second_number ]
do
	echo -n "2nd Number : "
	read second_number
done


case $option in 

    1 ) 
		echo -n "Result : " 
		echo `expr $first_number + $second_number`
	;;

    2 ) 
		echo -n "Result : " 
		echo `expr $first_number - $second_number`
	;;

    3 ) 
		echo -n "Result : " 
		echo `expr $first_number * $second_number`
	;;

    4 ) 
		echo -n "Result : " 
		echo `expr $first_number / $second_number`
	;;

esac
