# SHPY (Shell to Python Translator)
> _COMP2041 Assignment 1 – Semester 2, 2015_

A basic Shell to Python translator.

Written in Perl.

### Features
+ Basic Translation
+ Translate loops, if-statements, case, shell built-in command (echo, exit, read, cd, test, expr, echo -n, mv, chmod, ls, rm).
+ Efficient translation


### Introduction
Your task in this assignment to write a Shell compiler. Generally compilers take a high level language as input and output assembler, which can then can be directly executed. Your compiler will take shell scripts as input and output Python. Such a translation is useful because programmers sometimes convert shell scripts to Python. Most commonly this is done because extra functionality is needed, e.g. a GUI, and this functionality is much easier to implement in Python. Your task in this assignment is to automate this conversion. You must write a Perl program which takes as input a shell script and outputs an equivalent Python program.
The translation of some shell code to Python is straightforward. The translation of other shell code is difficult or infeasible. So your program will not be able to translate all shell code to Python. But a tool that performs only a partial translation of shell to Python could still be very useful.

You should assume the Python code output by your program will be subsequently read and modified by humans. In other word you have to output readable Python code. For example, you should aim to preserve variable names and comments.

Your compiler must be written in Perl. You should call your Perl program shpy.pl. It should operate as Unix filters typically do, read files specified on the command line and if none are specified it should read from standard input (Perl's <> operator does this for you).