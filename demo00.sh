#!/bin/sh

#
# cs2041, 15s2
#

# Partially taken from cs2041 lab week 3 (digits.sh)
# Test on : Echo, Shell-built-ins, Variable
# Demo : 0

start="start"
end="end"

echo "$start of command"

tr '0123456789' '<<<<<5>>>>'
date

echo "$end of command"
