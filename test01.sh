#!/bin/sh

#
# cs2041, 15s2
#

#
# This is a test of Level 1 features.
# Test : 1
#

cd /test /file
pwd -l

read username

if [ $username ]
then
	echo "Welcome, " $username;
else
	echo "Fail to recognize username : "
	exit 1234
fi

for object in *.sh 1 2 3 z? [abcd].txt
do
	echo $object
done

for file in *.*; do echo $file; done # One line for loop is annoying as always

filename_match=*.*
echo $filename # Annoying matching file data

