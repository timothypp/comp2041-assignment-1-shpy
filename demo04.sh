#!/bin/sh

#
# cs2041, 15s2
#

# Send an email containing image
# Test on : If, Special Variable, Exit, For, Variable, Test, While, Read, $(()), $()
# Demo : 4

#!/bin/sh

number_of_email_sent="0";

if test $# = 0
then
	exit 1
else
	for file in "$@"
	do
		#declare + reset the variables
		email=""
		message=""
		
		if [[ -f $file ]]
		then
			
			display "$file"
			
			#e-mail validation
			while [[ ! $email ]]
			do
				echo -n "Address to e-mail this image to? "
				read email
			done
			
			#message validation
			while [[ ! $message ]]
			do
				echo "Message to accompany image? "
				read message
			done

			echo $message 

			mutt -s $file -a $file -- $email #Email image to given address

			#echo "$file successfully sent to $email"
			number_of_email_sent=$(( $number_of_email_sent + 1 ))
		fi
	done
fi

echo "Number of email succesfulyy so far : $number_of_email_sent";
echo "As of : " $(date);
