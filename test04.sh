#!/bin/sh

#
# cs2041, 15s2
#

#
# This is a test of Level 4 features.
# Test : 4
#

echo $(ls -las $@)
echo $(( $1 + $2 - $3 \* $4 / $5 < $6 ))

echo "now support $(( )) && $( )"

`test 2 -lt 0` && echo you should never see this
`test 2 -lt 0` || echo but you might see this

`test 1 -eq 0` # It will not work definitely

pwd && echo success # I have no idea how to do this that's why i don't bother
pwd || echo some success # I have no idea how to do this that's why i don't bother


# Nasty case code 
case $option in 
    1 ) echo -n "Result : "; echo `expr $first_number + $second_number`;;
    2 ) echo -n "Result : "; echo `expr $first_number - $second_number`;;
    3 ) echo -n "Result : "; echo `expr $first_number * $second_number`;;
    4 ) echo -n "Result : "; echo `expr $first_number / $second_number` ;;
esac

mv *.sh file
mv "*.sh" *.txt
mv *.sh "*.txt"

chmod 700 [abcd].txt *.sh *.txt
chmod 070000000 *.*

ls -las

`ls lol`

$((rm all_of_the_file))

date >> date.txt

date > date_new.txt

date && pwd && ls || echo some success > some.txt

echo "lol"

`expr lol + 123123213`

cat << file.txt

cat < file.txt

# ABove will not work why :(

echo 1)
echo something ;;

echo case esac

