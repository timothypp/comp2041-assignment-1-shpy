#!/bin/sh

#
# cs2041, 15s2
#

#
# This is a test of Level 0 features.
# Test : 0
#

echo This seems to work
word1=Yes
word2=does
echo "'$word1' it '$word2' work"

ls -las

echo "#hashtag"									# echo a hashtag (same line)
echo "#hashtag diff";							# echo a hashtag (different line)

ls -l "./" test demo

echo We need to do something dude then we can relax
cd /
pwd

echo hello'123'bybye
echo hello"123"byebye

echo hello'$q'""

echo '`pwd`'

h="\\\\\\\"";									# Echo complicated things

echo $h

echo successful \\ fail is determined when "program is ran" '!' "\n" # \n will be printed how :(

a=\$a
b='\\$b'
c="\$c"
d="\\\\\$c"

echo $a $b $c '$d' "$e"

echo "I am cool
yet i am not
at the same time
just saying"


# A lot of cray escaping

lol #untranslatable
